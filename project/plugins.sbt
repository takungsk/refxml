addSbtPlugin("com.mojolly.scalate" % "xsbt-scalate-generator" % "0.4.2")

addSbtPlugin("org.scalatra.sbt" % "scalatra-sbt" % "0.2.0")

addSbtPlugin("com.github.mpeltonen" % "sbt-idea" % "1.2.0")

addSbtPlugin("io.spray" % "sbt-twirl" % "0.6.1")

addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "0.9.0")