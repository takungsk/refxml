# refxml #
xml を 再整形して表示する WEBアプリです。  


## Build & Run ##

```sh
$ cd refxml
$ sbt
> container:start
> browse
```
もし `browse` で ブラウザが起動しない時は 手動でブラウザを起動し [http://localhost:8080/](http://localhost:8080/) を開いてください。 

## ソースの変更を自動でビルドするには
```sh
$ sbt
> container:start
> ~; copy-resources;aux-compile
```


## standalone で実行する為の jar ファイルを作成するには  
``` sh
$ cd refxml
$ sbt clean assembly
```
target/scala-2.10 に refxml-assembly-0.1.0.jar が作成されます。

```
$ java -jar refxml-assembly-0.1.0.jar
```
で実行します。

http://localhost:8080/ or http://servername:8080/ にアクセスします。


