package refxml
/**
 * Created with IntelliJ IDEA.
 * User: takuya
 * Date: 2013/06/20
 * Time: 0:59
 * To change this template use File | Settings | File Templates.
 */
import org.eclipse.jetty.server.Server
import org.eclipse.jetty.servlet.{DefaultServlet, ServletContextHandler, ServletHolder}
import org.eclipse.jetty.webapp.WebAppContext
import org.scalatra.servlet.ScalatraListener

object JettyLauncher {
  def main(args: Array[String]) {
    val port = if(System.getenv("PORT") != null) System.getenv("PORT").toInt else 8080

    val server = new Server(port)
    val context = new WebAppContext()
    context setContextPath "/"
    context.setResourceBase("src/main/webapp")
    context.addEventListener(new ScalatraListener)
    context.addServlet(classOf[MyDefaultServlet], "/")
    context.addServlet(new ServletHolder(new MyDefaultServlet), "/assets/*")

    server.setHandler(context)

    server.start
    server.join
  }
}
