package refxml

import org.scalatra.test.scalatest._
import org.scalatest.FunSuite

class RefxmlServletTests extends ScalatraSuite with FunSuite {
  addServlet(classOf[refxml], "/*")

  test("simple get") {
    get("/refxml") {
      status should equal (200)
      body should include ("Reformat XML")
    }

    get("/") {
      status should equal (302)
    }
  }

  test("js page get") {
    get("/jsxml") {
      status should equal (200)
      body should include ("sample")
    }
  }

  test("simple post") {
    post("/refxml", "xmlarea" -> "<a>sample</a>") {
      status should equal (200)
      body should include ("sample")
    }

    post("/refxml", "xmlarea" -> "not xml style") {
      status should equal (200)
      body should include ("ERROR:")
    }
  }
}